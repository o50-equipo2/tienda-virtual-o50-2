-- Script para limpiar y carga datos a la DB del proyecto tienda virtual.
-- Mintic2022 ciclo 3 O50-2.
-- Autor: Leonardo Fabio Mercado Benítez.

-- Usando la DB tiendaVirtual.
USE tiendaVirtual;
-- Eliminando datos de la tabla Movimiento.
DELETE FROM movimiento WHERE idMovimiento>0;
-- Eliminando datos de la tabla Producto.
DELETE FROM producto WHERE idProducto>0;
-- Eliminando datos de la tabla Categoria.
DELETE FROM categoria WHERE idCategoria>0;


-- Insertando datos en la tabla Categoria.
INSERT INTO categoria(idCategoria,categoria)
VALUES
	(1,'Licores'),
	(2,'Golosinas'),
	(3,'Snacks'),
	(4,'Productos de aseo');
    
-- Insertando datos en la tabla Producto.
INSERT INTO producto(idProducto,
					 idCategoria,
                     codigo,
                     cantidad,
                     nombre,
                     fechaVencimiento,
                     valorCosto,
                     valorVenta)
VALUES
	(1,1,'ABC123',38,'Cerveza - Poker - Pack x6 Latas 330 ml','2022-11-03',12612,14126),
	(2,1,'ABC124',44,'Aguardiente Tradicional - Antioqueño Rojo - Botella 375 ml','2022-11-03',16686,18689),
	(3,1,'ABC125',33,'Aguardiente Sin Azúcar 24° - Antioqueño Verde - Botella 375 ml','2022-11-03',15528,17392),
	(4,1,'ABC126',34,'Aguardiente Sin Azúcar - Antioqueño Azul - Botella 375 ml','2022-11-03',17974,20131),
	(5,1,'ABC127',26,'Aguardiente Tradicional - Antioqueño Rojo - Tetra pak 260 ml','2022-11-03',10512,11773),
	(6,1,'ABC128',27,'Vino Rosado   - Rosaleda   - Botella 750 ml','2022-11-03',11522,12904),
	(7,1,'ABC129',41,'Tequila Reposado Especial - Jose Cuervo - Botella 750 ml','2022-11-03',69738,78107),
	(8,1,'ABC130',20,'Vino Espumoso Sparkling Rose - Jp Chenet - Botella 750 ml','2022-11-03',46063,51591),
	(9,1,'ABC131',23,'Vino Espumoso Fashion Strawberry - Jp Chenet   - Botella 750 ml','2022-11-03',56179,62920),
	(10,1,'ABC132',43,'Cerveza - Corona - Display x6 Botellas 355 ml','2022-11-03',15876,17782),
	(11,1,'ABC133',42,'Cerveza  - Costeña Bacana - Pack x6 Latas 330 ml','2022-11-03',6907,7736),
	(12,1,'ABC134',21,'Refajo - Cola & Pola - Botella pet 1500 ml','2022-11-03',2649,2967),
	(13,1,'ABC135',48,'Cerveza - Budweiser - PACK x6 Latas 269 ml','2022-11-03',8575,9604),
	(14,1,'ABC136',46,'Cerveza - Aguila - Display x6 Latas 330 ml','2022-11-03',8300,9296),
	(15,2,'ABC137',43,'Chocolate en crema - Nucita - Display x18 Unidades 14 g','2022-09-23',6835,7655),
	(16,2,'ABC138',28,'Galleta  De Leche    - Muuu   - Paquete 18 uds.','2022-09-23',2845,3186),
	(17,2,'ABC139',40,'Gomitas Tiburón - Grissly - Paquete 55 g','2022-09-23',828,927),
	(18,2,'ABC140',47,'Goma rellena   - Grissly   - Paquete 80 g','2022-09-23',1314,1472),
	(19,2,'ABC141',24,'Chocolate relleno - Chocobreak Ball - Bolsa 50 uds.','2022-09-23',3164,3544),
	(20,2,'ABC142',33,'Chocodisk Dandy - Kick - Caja 18 uds.','2022-09-23',8568,9596),
	(21,2,'ABC143',24,'Galleta Wafer - Nucita - Bolsa 8 uds.','2022-09-23',2782,3116),
	(22,2,'ABC144',31,'Chocodisk Dandy - Kick - Ristra 6 uds.','2022-09-23',2882,3228),
	(23,2,'ABC145',48,'Menta helada - Colombina - Bolsa x100 Unidades 4 g','2022-09-23',4744,5313),
	(24,2,'ABC146',20,'Chocolate blanco de cookies & cream - Chocobreak - Bolsa 30 Und','2022-09-23',4369,4894),
	(25,2,'ABC147',24,'Chupeta tipitin - Pirulito - Bolsa 24 uds.','2022-09-23',2908,3257),
	(26,2,'ABC148',39,'Gomitas Osos   - Grissly   - Paquete 80 g','2022-09-23',1314,1472),
	(27,2,'ABC149',31,'Fruticas cruji masticables   - Colombina   - Bolsa 100 uds.','2022-09-23',5590,6261),
	(28,2,'ABC150',21,'Galletas wafer vainilla - Capri - Bolsa x24 Unidades 12 g','2022-09-23',3879,4344),
	(29,2,'ABC151',40,'Caramelo duro - Morita Colombina - Bolsa 24 uds.','2022-09-23',3394,3802),
	(30,3,'ABC152',35,'Cereal - Zucaritas Kelloggs - Paquete 30 g','2022-09-24',555,621),
	(31,3,'ABC153',33,'Papas sabor a queso - Pringles - Tarro 124 g','2022-09-24',7552,8458),
	(32,3,'ABC154',25,'Papas  original - Pringles - Tarro 67 g','2022-09-24',3531,3955),
	(33,3,'ABC155',49,'Papas queso - Pringles - Tarro 37 g','2022-09-24',3045,3410),
	(34,3,'ABC156',47,'Cereal - Zucaritas - Paquete 300 g','2022-09-24',7770,8703),
	(35,3,'ABC157',34,'Ponqué redondo tradicional - Ramo Tradicional - Unidad 230 g','2022-09-24',3166,3546),
	(36,3,'ABC158',42,'Pasabocas natural - Maizitos - Unidad 215 g','2022-09-24',3504,3924),
	(37,3,'ABC159',35,'Ponqué mini cubierto de chocolate - Chocoramo - Paquete 400 g','2022-09-24',8761,9812),
	(38,3,'ABC160',30,'Ponqué Cubierto De Chocolate Forti - Chocoramo - Paquete x4 Bolsas 160 g','2022-09-24',3243,3632),
	(39,3,'ABC161',24,'Pasabocas picante - Tostacos - Unidad 200 g','2022-09-24',3504,3924),
	(40,3,'ABC162',46,'Plátano maduro - Ramo - Paquete x8 Unidades 40 g','2022-09-24',9121,10215),
	(41,3,'ABC163',21,'Pasabocas Plátanoverde - Ramo - Paquete x8 Unidades 40 g','2022-09-24',9121,10215),
	(42,3,'ABC164',20,'Nachos sabor natural - Ramo - Unidad 190 g','2022-09-24',3504,3924),
	(43,3,'ABC165',38,'Nachos queso - Ramo - Unidad 190 g','2022-09-24',3504,3924),
	(44,3,'ABC166',30,'Pasabocas de queso - Tostacos - Unidad 200 g','2022-09-24',3504,3924),
	(45,4,'ABC167',28,'Suavizante aroma floral - Aromatel - Doypack 400 ml','2022-10-17',1942,2175),
	(46,4,'ABC168',38,'Suavizante aromatel floral - Aromatel - Doypack 180 ml','2022-10-17',906,1014),
	(47,4,'ABC169',36,'Suavizante ropa manzana verde - Aromatel - Doypack 400 ml','2022-10-17',1942,2175),
	(48,4,'ABC170',34,'Detergente en polvo floral - Fab - Bolsa 225 g','2022-10-17',1683,1885),
	(49,4,'ABC171',46,'Detergente En Polvo Multiusos - 3D - Bolsa 500 g','2022-10-17',2525,2828),
	(50,4,'ABC172',47,'Suavizante aroma manzana - Aromatel - Doypack 180 ml','2022-10-17',906,1015),
	(51,4,'ABC173',49,'Detergente en barra multiusos - Fab - Barra 300 g','2022-10-17',1909,2138),
	(52,4,'ABC174',38,'Detergente en polvo multiusos - 3D - Bolsa 125 g','2022-10-17',906,1015),
	(53,4,'ABC175',23,'Detergente en polvo  campos de lavanda - Rindex - Unidad 450 g','2022-10-17',2400,2688),
	(54,4,'ABC176',36,'Detergente En Polvo Suavizante - Rindex 10 Multibeneficios - Bolsa 500 g','2022-10-17',2151,2410),
	(55,4,'ABC177',26,'Detergente Líquido Revitacolor - Ariel Revitacolor - Unidad 1200 ml','2022-10-17',10001,11201),
	(56,4,'ABC178',45,'Detergente en polvo  regular - Ariel - Unidad 2000 g','2022-10-17',12545,14051),
	(57,4,'ABC179',39,'Detergente en polvo  flores para mis amores - Rindex - Unidad 1000 g','2022-10-17',4658,5217),
	(58,4,'ABC180',43,'Detergente en polvo  multibeneficios - Rindex 10 - Bolsa 250 g','2022-10-17',1225,1372),
	(59,4,'ABC181',28,'Detergente En Polvo Suavizante - Rindex 10 Multibeneficios - Bolsa 125 g','2022-10-17',605,677);

-- Datos Tabla Movimiento
INSERT INTO `tiendavirtual`.`movimiento` ( `idMovimiento`, `idProducto`, `cantidad`, `valor`,`fecha`, `tipo`) VALUES ( 1, 1, 25, 8500, '2022-02-15', 'COMPRA');
INSERT INTO `tiendavirtual`.`movimiento` ( `idMovimiento`, `idProducto`, `cantidad`, `valor`,`fecha`, `tipo`) VALUES (2, 1, 10, 12300,  '2022-02-25', 'VENTA');
INSERT INTO `tiendavirtual`.`movimiento` ( `idMovimiento`, `idProducto`, `cantidad`, `valor`,`fecha`, `tipo`) VALUES (3, 2, 12, 21900, '2022-06-04', 'COMPRA');
INSERT INTO `tiendavirtual`.`movimiento` ( `idMovimiento`, `idProducto`, `cantidad`, `valor`,`fecha`, `tipo`) VALUES (4, 3, 45, 7550, '2022-06-13', 'COMPRA');
INSERT INTO `tiendavirtual`.`movimiento` (`idMovimiento`, `idProducto`, `cantidad`, `valor`, `fecha`, `tipo`) VALUES (5, 3, 5, 28000, '2022-07-01', 'VENTA');
