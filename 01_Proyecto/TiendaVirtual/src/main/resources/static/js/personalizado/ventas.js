// Call the dataTables jQuery plugin
$(document).ready(function() {
  //PVR cargar todo antes de llamar DataTable()
  cargarMovimientos();
  $('#movimientoTable').DataTable();
});

async function cargarMovimientos() {
//PVR: Traer el Json de nuestra api como entrada a la tabla. El Body en GET no se necesita
//    ejemplo de fetch extraido de https://www.thiscodeworks.com/javascript-fetch-post-json-data-stack-overflow-javascript/61eba6c270147b00155329c1
  const request = await fetch('api/ventas', {
    method: 'GET',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    }
  });
  const movimientos = await request.json();
  let tablaHTML= '';
  fec = new Date();
  for( let movimiento of movimientos){
    fec = movimiento.fecha;
    let movimientoHTML = '<tr>'
                    + '<td>' + movimiento.idMovimiento + '</td>'
                    +'<td>' + movimiento.idProducto + '</td>'
                    + '<td>' + movimiento.cantidad + '</td>'
                    + '<td>' + fec.toString().substring(0,10) + '</td>'
                    + '<td>' + movimiento.tipo + '</td>'
                    + '<td>'
                    + '<a href="" role="button" onclick="mostrarMovimiento('+ movimiento.idMovimiento +')" class="btn btn-info btn-circle btn-sm" data-toggle="modal"><i class="fas fa-search"></i></a>'
                    + '<a href="" role="button" onclick="mostrarCambioMovimiento('+ movimiento.idMovimiento +')" class="btn btn-warning btn-circle btn-sm" data-toggle="modal"><i class="fas fa-edit"></i></a>'
                    + '<a href=""' +' onclick="borraMovimiento('+ movimiento.idMovimiento + ')" class="btn btn-danger btn-circle btn-sm"><i class="fas fa-trash"></i></a>'
                    + '</td></tr>';

    tablaHTML += movimientoHTML;
  }
  document.querySelector('#movimientoTable tbody').outerHTML = tablaHTML;
  //console.log(movimientos);

}
function cerrarModal(){
    location.reload();
}

function mostrarCrear() {
    document.querySelector('#crud_mov_botones').outerHTML = '<div class="mb-4 d-flex flex-row justify-content-center"><button type="button" class="btn btn-default" data-dismiss="modal" onclick="cerrarModal()">Cancelar</button><button type="button" class="btn btn-primary" onclick="crearMovimiento()">Agregar</button></div>';
    document.querySelector('#crud_titulo').outerHTML = 'Crear';
    $("#crud_movimiento").modal("show");
}
function mostrarMovimiento(id) {
    document.querySelector('#crud_mov_botones').outerHTML = '<div class="mb-4 d-flex flex-row justify-content-center"><button type="button" class="btn btn-primary" data-dismiss="modal" onclick="cerrarModal()">Aceptar</button></div>';
    document.querySelector('#crud_titulo').outerHTML = 'Detalle';
  //sessionStorage.setItem("id", id);
  $.ajax({
    url: "api/ventas/" + id,
    method: "GET",
    success: function (data) {
      $("#idMovimiento").val(data.idMovimiento);
      $("#idProducto").val(data.idProducto);
      $("#cantidad").val(data.cantidad);
      $("#fecha").val(data.fecha);
      fec = new Date();
      fec = data.fecha;
      document.getElementById("fecha").value = fec.toString().substring(0,10);
      $("#tipo").val(data.tipo);
      $("#crud_movimiento").modal("show");
    },
    error: function (request, msg, error) {
      // handle failure
    },
  });
}

function mostrarCambioMovimiento(id) {
    document.querySelector('#crud_mov_botones').outerHTML = '<div class="mb-4 d-flex flex-row justify-content-center"><button type="button" class="btn btn-default" data-dismiss="modal" onclick="cerrarModal()">Cancelar</button><button type="button" class="btn btn-primary" onclick="actualizarMovimiento('+ id + ')">Modificar</button></div>';
    document.querySelector('#crud_titulo').outerHTML = 'Editar';
  //sessionStorage.setItem("id", id);
  $.ajax({
    url: "api/ventas/" + id,
    method: "GET",
    success: function (data) {
      $("#idMovimiento").val(data.idMovimiento);
      $("#idProducto").val(data.idProducto);
      $("#cantidad").val(data.cantidad);
      $("#fecha").val(data.fecha);
      fec = new Date();
      fec = data.fecha;
      document.getElementById("fecha").value = fec.toString().substring(0,10);
      $("#tipo").val(data.tipo);

      $("#crud_movimiento").modal("show");
    },
    error: function (request, msg, error) {
      // handle failure
    },
  });
}

async function borraMovimiento(id) {
    //alert(id);
    if(confirm("Confirma el borrado?")){
        //PVR: Traer el Json de nuestra api como entrada a la tabla. El Body en GET no se necesita
        //    ejemplo de fetch extraido de https://www.thiscodeworks.com/javascript-fetch-post-json-data-stack-overflow-javascript/61eba6c270147b00155329c1
        const request = await fetch('api/ventas/'+id, {
            method: 'DELETE',
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json'
            }
        });
        //location.reload();
    }
}

function crearMovimiento() {
  var movimiento = {
    idProducto: $("#idProducto").val(),
    cantidad: $("#cantidad").val(),
    fecha: $("#fecha").val()+'T05:00:00.000+00:00',
    tipo: $("#tipo").val(),
  };

  var json = JSON.stringify(movimiento);

  $.ajax({
    type: "POST",
    url: "api/ventas",
    data: json,
    contentType: "application/json; charset=utf-8",
    success: function (data) {
      $("#crud_movimiento").modal("hide");
      cargarMovimientos();

      $("#idProducto").val(),
      $("#cantidad").val(),
      $("#fecha").val(),
      $("#tipo").val("");
      location.reload();
    },
  });
}
// Crear editarMovimiento tipo GET y guardar variable en memoria
function actualizarMovimiento(id) {
  var movimiento = {
    idMovimiento: $("#idMovimiento").val(),
    idProducto: $("#idProducto").val(),
    cantidad: $("#cantidad").val(),
    fecha: $("#fecha").val()+'T05:00:00.000+00:00',
    tipo: $("#tipo").val(),
  };

  var json = JSON.stringify(movimiento);

  $.ajax({
    type: "PUT",
    url: "api/ventas/"+id,
    data: json,
    contentType: "application/json; charset=utf-8",
    success: function (data) {
      $("#crud_movimiento").modal("hide");
      cargarMovimientos();

      $("#idProducto").val(),
      $("#cantidad").val(),
      $("#fecha").val(),
      $("#tipo").val("");
      location.reload();
    },
  });
}
