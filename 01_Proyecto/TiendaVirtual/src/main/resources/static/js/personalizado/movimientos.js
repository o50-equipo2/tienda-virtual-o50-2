// Call the dataTables jQuery plugin
$(document).ready(function() {
  //PVR cargar todo antes de llamar DataTable()
  cargarMovimientos();
  $('#movimientoTable').DataTable();
});

function mostrarCrear() {
    document.querySelector('#crud_mov_botones').outerHTML = '<div class="mb-4 d-flex flex-row justify-content-center"><button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button><button type="button" class="btn btn-primary" onclick="crearMovimiento()">Agregar</button></div>';
    $("#crud_movimiento").modal("show");
}

function mostrarMovimiento(id) {
    document.querySelector('#crud_mov_botones').outerHTML = '<div class="mb-4 d-flex flex-row justify-content-center"><button type="button" class="btn btn-primary" data-dismiss="modal">Cancelar</button></div>';
  //sessionStorage.setItem("id", id);
  $.ajax({
    url: "api/movimientos/" + id,
    method: "GET",
    success: function (data) {
      $("#idMovimiento").val(data.idMovimiento);
      $("#idProducto").val(data.idProducto);
      $("#cantidad").val(data.cantidad);
      $("#fecha").val(data.fecha);
      $("#tipo").val(data.tipo);

      $("#crud_movimiento").modal("show");
    },
    error: function (request, msg, error) {
      // handle failure
    },
  });



async function cargarMovimientos() {
//PVR: Traer el Json de nuestra api como entrada a la tabla. El Body en GET no se necesita
//    ejemplo de fetch extraido de https://www.thiscodeworks.com/javascript-fetch-post-json-data-stack-overflow-javascript/61eba6c270147b00155329c1
  const request = await fetch('api/movimientos', {
    method: 'GET',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    }
  });
  const movimientos = await request.json();
  let tablaHTML= '';
  for( let movimiento of movimientos){
    let movimientoHTML = '<tr>'
                    + '<td>' + movimiento.idMovimiento + '</td>'
                    +'<td>' + movimiento.idProducto + '</td>'
                    + '<td>' + movimiento.cantidad + '</td>'
                    + '<td>' + movimiento.fecha + '</td>'
                    + '<td>' + movimiento.tipo + '</td>'
                    + '<td>'
                    +'<a href=""' +' onclick="mostrarMovimiento('+ movimiento.idMovimiento +')" class="btn btn-info btn-circle btn-sm"><i class="fas fa-search"></i></a>'
                    + '<a href="#'+ movimiento.idMovimiento +'"' +' class="btn btn-warning btn-circle btn-sm"><i class="fas fa-edit"></i></a>'
                    + '<a href=""' +' onclick="borraMovimiento('+ movimiento.idMovimiento + ')" class="btn btn-danger btn-circle btn-sm"><i class="fas fa-trash"></i></a>'
                    + '</td></tr>';
    tablaHTML += movimientoHTML;
  }
  document.querySelector('#movimientoTable tbody').outerHTML = tablaHTML;
  //console.log(movimientos);

}

async function borraMovimiento(id) {
    //alert(id);
    if(confirm("Confirma el borrado?")){
        //PVR: Traer el Json de nuestra api como entrada a la tabla. El Body en GET no se necesita
        //    ejemplo de fetch extraido de https://www.thiscodeworks.com/javascript-fetch-post-json-data-stack-overflow-javascript/61eba6c270147b00155329c1
        const request = await fetch('api/movimientos/'+id, {
            method: 'DELETE',
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json'
            }
        });
        //location.reload();
    }
}

function crearMovimiento() {
  var movimiento = {
    idProducto: $("#idProducto").val(),
    cantidad: $("#cantidad").val(),
    fecha: $("#fecha").val(),
    tipo: $("#tipo").val(),
  };

  var json = JSON.stringify(movimiento);

  $.ajax({
    type: "POST",
    url: "api/movimientos",
    data: json,
    contentType: "application/json; charset=utf-8",
    success: function (data) {
      $("#crud_movimiento").modal("hide");
      cargarMovimientos();

      $("#idProducto").val(),
      $("#cantidad").val(),
      $("#fecha").val(),
      $("#tipo").val("");
      location.reload();
    },
  });
}
// Crear editarMovimiento tipo GET y guardar variable en memoria
function actualizarMovimiento(id) {
  var movimiento = {
    idProducto: $("#idProducto").val(),
    cantidad: $("#cantidad").val(),
    fecha: $("#fecha").val(),
    tipo: $("#tipo").val(),
  };

  var json = JSON.stringify(movimiento);

  $.ajax({
    type: "PUT",
    url: "api/movimientos/"+id,
    data: json,
    contentType: "application/json; charset=utf-8",
    success: function (data) {
      $("#crud_movimiento").modal("hide");
      cargarMovimientos();

      $("#idProducto").val(),
      $("#cantidad").val(),
      $("#fecha").val(),
      $("#tipo").val("");
      location.reload();
    },
  });
}


