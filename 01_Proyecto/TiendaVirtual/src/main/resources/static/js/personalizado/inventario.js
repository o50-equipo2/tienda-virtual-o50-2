// Call the dataTables jQuery plugin
$(document).ready(function() {
  cargarProductosInventario();
});

let codigosProductos = [];
let productosInventario;

function cargarProductosInventario(){
  (async () => {
    const request = await fetch('api/productos', {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      }
    });
    const productos = await request.json();
    productosInventario = productos;
    for(let producto of productos){
      codigosProductos.push(producto.codigo);
    }
    cargarDatosProductos(productos);
  })();
}

function cargarDatosProductos(productos){
  $("#tablaBody").empty();
  for(let producto of productos){
    let botonEliminar = '</a> <a href="#" onclick="eliminarUsuario('+ producto.idProducto +')" class="btn btn-danger btn-circle" data-toggle="tooltip" data-placement="bottom" title="Elimina aquí tus productos"> <i class="fas fa-trash"></i> </a>';
    let botonEditar = '</a> <a href="#" onclick="cargarDatosProducto('+ producto.idProducto +')" class="btn btn-warning btn-circle" data-toggle="modal" data-target="#modalEditar"data-toggle="tooltip" data-placement="bottom" title="Edita aquí tus productos"> <i class="fas fa-edit"></i> </a>';
    $("#tablaBody").append(
        '<tr>' +
        '<td style="text-align: left !important;">'+ producto.nombre +'</td>' +
        '<td>'+ producto.cantidad +'</td>' +
        '<td>'+ producto.valorCosto +'</td>' +
        '<td>'+ producto.valorVenta +'</td>' +
        '<td>'+ producto.fechaVencimiento.split("T")[0] +'</td>' +
        '<td>'+ botonEditar + botonEliminar +'</td>' +
        '</tr>');
  }
}

$('#buscador').keyup(function () {
  let productoBuscar = $('#buscador').val();
  filtrarProductos(productoBuscar)
});

function filtrarProductos(texto){
    let objetivo = texto.toLowerCase()
    $("#tablaBody").empty();
    for(let producto of productosInventario){
      let nombre = producto.nombre.toLowerCase();
      if(nombre.indexOf(objetivo) !== -1){
        let botonEliminar = '</a> <a href="#" onclick="eliminarUsuario('+ producto.idProducto +')" class="btn btn-danger btn-circle" data-toggle="tooltip" data-placement="bottom" title="Elimina aquí tus productos"> <i class="fas fa-trash"></i> </a>';
        let botonEditar = '</a> <a href="#" onclick="cargarDatosProducto('+ producto.idProducto +')" class="btn btn-warning btn-circle" data-toggle="modal" data-target="#modalEditar"data-toggle="tooltip" data-placement="bottom" title="Edita aquí tus productos"> <i class="fas fa-edit"></i> </a>';
        $("#tablaBody").append(
            '<tr>' +
            '<td style="text-align: left !important;">'+ producto.nombre +'</td>' +
            '<td>'+ producto.cantidad +'</td>' +
            '<td>'+ producto.valorCosto +'</td>' +
            '<td>'+ producto.valorVenta +'</td>' +
            '<td>'+ producto.fechaVencimiento.split("T")[0] +'</td>' +
            '<td>'+ botonEditar + botonEliminar +'</td>' +
            '</tr>');
      }
    }
    if($("#tablaBody").contents().length === 0){
      $("#tablaBody").append(
          '<tr>' +
          '<td style="text-align: left !important;">'+ 'No se encontraron productos con este nombre' +'</td>' +
          '<td>'+ '</td>' +
          '<td>'+'</td>' +
          '<td>'+ '</td>' +
          '<td>'+'</td>' +
          '<td>'+ '</td>' +
          '</tr>');
    }
}

function eliminarUsuario(id){

  if(!confirm('¿Seguro que desea eliminar este producto?')){
    return;
  }

  (async () => {
    const request = await fetch('api/productos/'+id, {
      method: 'DELETE',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      }
    });
    cargarProductosInventario();
  })();
}

function cargarDatosProducto(idProducto){
  (async () => {
    const request = await fetch('api/productos/'+idProducto, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      }
    });
    const producto = await request.json();
    $("#idProducto").val(producto.idProducto);
    $("#idCategoria").val(producto.idCategoria);
    $("#codigo").val(producto.codigo);
    $("#cantidad").val(producto.cantidad);
    $("#valorVenta").val(producto.valorVenta);
    $("#nombre").val(producto.nombre);
    $("#fechaVencimiento").val(producto.fechaVencimiento);
    $("#valorCosto").val(producto.valorCosto);
    cargarCategorias(producto.idCategoria);
  })();
}

function cargarCategorias(id){
  (async () => {
    const request = await fetch('api/categorias/', {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      }
    });
    const categorias = await request.json();
    $("#idCategoria").empty();
    for(i=0;i<categorias.length;i++){
      if(categorias[i].idCategoria===id){
        $('#idCategoria').append('<option value="' + categorias[i].idCategoria + '">' + categorias[i].categoria + '</option>');
      }
    }
    for(i=0;i<categorias.length;i++){
      if(!(categorias[i].idCategoria===id)){
        $('#idCategoria').append('<option value="' + categorias[i].idCategoria + '">' + categorias[i].categoria + '</option>');
      }
    }
  })();
}

function cargarTodasLasCategorias(){
  (async () => {
    const request = await fetch('api/categorias/', {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      }
    });
    const categorias = await request.json();
    $("#idCategoriaCrear").empty();
    for(i=0;i<categorias.length;i++){
      $('#idCategoriaCrear').append('<option value="' + categorias[i].idCategoria + '">' + categorias[i].categoria + '</option>');
    }
  })();
}


$("#btnEditar").click(function (){
  let idProducto = $("#idProducto").val();
  let idCategoria = $("#idCategoria").val();
  let codigo = $("#codigo").val();
  let cantidad = $("#cantidad").val();
  let valorVenta = $("#valorVenta").val();
  let nombre = $("#nombre").val();
  let fechaVencimiento = $("#fechaVencimiento").val();
  let valorCosto = $("#valorCosto").val();

  if(cantidad < 0){
    alert("No puede editar un producto con cantidad negativa.");
    return;
  }

  if(valorVenta <= 0){
    alert("No puede editar un producto sin un valor de venta.");
    return;
  }

  if(nombre.length === 0){
    alert("No puede editar un producto sin asignarle un nombre.");
    return;
  }

  (async () => {
    const request = await fetch('api/productos/'+idProducto, {
      method: 'PUT',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        'idProducto': idProducto,
        'idCategoria': idCategoria,
        'codigo': codigo,
        'cantidad': cantidad,
        'nombre': nombre,
        'fechaVencimiento': fechaVencimiento,
        'valorCosto': valorCosto,
        'valorVenta': valorVenta
      })
    });
    const respuestaEdicion = request.status;
    if(respuestaEdicion === 200){
      alert("Producto editado correctamente");
      cargarProductosInventario();
      $("#modalEditar").modal('hide');
    }else{
      alert("Ocurrio un error al editar el producto, vuelve a intentarlo en unos minutos.");
    }
  })();
});


$("#btnModalCrear").click(function () {
  cargarTodasLasCategorias();
});



$('#btnCrearProducto').click(function () {
  let idProducto = 0;
  let idCategoria = $("#idCategoriaCrear").val();
  let codigo = $("#codigoCrear").val();
  let cantidad = $("#cantidadCrear").val();
  let valorVenta = $("#valorVentaCrear").val();
  let nombre = $("#nombreCrear").val();
  let fechaVencimiento = $("#fechaVencimientoCrear").val()+'T05:00:00.000+00:00';
  let valorCosto = $("#valorCostoCrear").val();
  let productoNuevo = {
    'idProducto': idProducto,
    'idCategoria': idCategoria,
    'codigo': codigo,
    'cantidad': cantidad,
    'valorVenta': valorVenta,
    'nombre': nombre,
    'fechaVencimiento': fechaVencimiento,
    'valorCosto': valorCosto,
  }

  if(codigo.length === 0){
    alert("No puede Crear un producto sin un código de producto.");
    return;
  }

  if(cantidad <= 0){
    alert("No puede Crear un producto sin una cantidad.");
    return;
  }

  if(nombre.length === 0){
    alert("No puede Crear un producto sin asignarle un nombre.");
    return;
  }

  if(fechaVencimiento === 'T05:00:00.000+00:00'){
    alert("No puede Crear un producto sin asignarle una fecha de vencimiento.");
    return;
  }

  if(valorCosto <= 0){
    alert("No puede Crear un producto sin un valor de costo.");
    return;
  }

  if(valorVenta <= 0){
    alert("No puede Crear un producto sin un valor de venta.");
    return;
  }

  if(codigosProductos.includes(codigo)){
    alert("El producto con este código ya se encuentra creado en DB");
    return;
  }else{
    crearProducto(productoNuevo);
  }


});

function crearProducto(producto){
  (async () => {
    const request = await fetch('api/productos/', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        'idProducto': producto.idProducto,
        'idCategoria': producto.idCategoria,
        'codigo': producto.codigo,
        'cantidad': producto.cantidad,
        'nombre': producto.nombre,
        'fechaVencimiento': producto.fechaVencimiento,
        'valorCosto': producto.valorCosto,
        'valorVenta': producto.valorVenta
      })
    });
    const respuestaCreacion = request.status;
    if(respuestaCreacion === 200){
      alert("Producto Creado correctamente");
      cargarProductosInventario();
      $("#modalCrear").modal('hide');
    }else{
      alert("Ocurrio un error al Crear el producto, vuelve a intentarlo en unos minutos.");
    }
  })();

};