// Call the dataTables jQuery plugin
$(document).ready(function() {
  cargarProductosMovimiento();
});

function cargarProductosMovimiento(){
  (async () => {
    const request = await fetch('api/movproductoventas', {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      }
    });
    const productosMovimiento = await request.json();

    $("#tablaBody").empty();
    for(let producto of productosMovimiento){
      let botonEliminar = '</a> <a href="#" onclick="borraMovimiento('+ producto.idProducto +')" class="btn btn-danger btn-circle" data-toggle="tooltip" data-placement="bottom" title="Elimina aquí tus productos"> <i class="fas fa-trash"></i> </a>';
      let botonEditar = '</a> <a href="#" onclick="cargarDatosProducto('+ producto.idProducto +')" class="btn btn-warning btn-circle" data-toggle="modal" data-target="#modalEditar"data-toggle="tooltip" data-placement="bottom" title="Edita aquí tus productos"> <i class="fas fa-edit"></i> </a>';
      $("#tablaBody").append(
          '<tr>' +
          '<td style="text-align: left !important;">'+ producto.nombre +'</td>' +
          '<td>'+ producto.categoria +'</td>' +
          '<td>'+ producto.cantidad +'</td>' +
          '<td>'+ producto.valor +'</td>' +
          '<td>'+ producto.fecha.split("T")[0] +'</td>' +
          '<td>'+ botonEditar + botonEliminar +'</td>' +
          '</tr>');
    }
  })();
}

async function borraMovimiento(id) {
    //alert(id);
    if(confirm("Confirma el borrado?")){
        //PVR: Traer el Json de nuestra api como entrada a la tabla. El Body en GET no se necesita
        //    ejemplo de fetch extraido de https://www.thiscodeworks.com/javascript-fetch-post-json-data-stack-overflow-javascript/61eba6c270147b00155329c1
        const request = await fetch('api/movimientos/'+id, {
            method: 'DELETE',
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json'
            }
        });
        //location.reload();
    }
}

function cargarDatosProducto(idProducto){
  (async () => {
    const request = await fetch('api/productos/'+idProducto, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      }
    });
    const producto = await request.json();
    $("#idProducto").val(producto.idProducto);
    $("#idCategoria").val(producto.idCategoria);
    $("#codigo").val(producto.codigo);
    $("#cantidad").val(producto.cantidad);
    $("#valorVenta").val(producto.valorVenta);
    $("#nombre").val(producto.nombre);
    $("#fechaVencimiento").val(producto.fechaVencimiento);
    $("#valorCosto").val(producto.valorCosto);
    cargarCategorias(producto.idCategoria);
  })();
}

function cargarCategorias(id){
  (async () => {
    const request = await fetch('api/categorias/', {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      }
    });
    const categorias = await request.json();
    $("#idCategoria").empty();
    for(i=0;i<categorias.length;i++){
      if(categorias[i].idCategoria===id){
        $('#idCategoria').append('<option value="' + categorias[i].idCategoria + '">' + categorias[i].categoria + '</option>');
      }
    }
    for(i=0;i<categorias.length;i++){
      if(!(categorias[i].idCategoria===id)){
        $('#idCategoria').append('<option value="' + categorias[i].idCategoria + '">' + categorias[i].categoria + '</option>');
      }
    }
  })();
}

$("#btnEditar").click(function (){
  let Movimiento = $("#idMovimiento").val();
  let idProducto = $("#idProducto").val();
  let idCategoria = $("#idCategoria").val();
  let codigo = $("#codigo").val();
  let cantidad = $("#cantidad").val();
  let valorVenta = $("#valorVenta").val();
  let nombre = $("#nombre").val();
  let fechaVencimiento = $("#fechaVencimiento").val();
  let valorCosto = $("#valorCosto").val();

  if(cantidad < 0){
    alert("No puede editar un producto con cantidad negativa.");
    return;
  }

  if(valorVenta <= 0){
    alert("No puede editar un producto sin un valor de venta.");
    return;
  }

  if(nombre.length === 0){
    alert("No puede editar un producto sin asignarle un nombre.");
    return;
  }

  (async () => {
    const request = await fetch('api/productos/'+idProducto, {
      method: 'PUT',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        'idProducto': idProducto,
        'idCategoria': idCategoria,
        'codigo': codigo,
        'cantidad': cantidad,
        'nombre': nombre,
        'fechaVencimiento': fechaVencimiento,
        'valorCosto': valorCosto,
        'valorVenta': valorVenta
      })
    });
    const respuestaEdicion = request.status;
    if(respuestaEdicion === 200){
      alert("Producto editado correctamente");
      cargarProductosMovimiento();
      $("#modalEditar").modal('hide');
    }else{
      alert("Ocurrio un error al editar el producto, vuelve a intentarlo en unos minutos.");
    }
  })();
});