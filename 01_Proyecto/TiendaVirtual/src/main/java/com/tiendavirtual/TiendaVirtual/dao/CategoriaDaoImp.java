package com.tiendavirtual.TiendaVirtual.dao;

import com.tiendavirtual.TiendaVirtual.models.Categoria;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
@Transactional
public class CategoriaDaoImp implements CategoriaDao{

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Categoria> getCategorias(){
        String query = "FROM Categoria";
        return entityManager.createQuery(query).getResultList();
    }

    @Override
    public Categoria getCategoriaById(int id){
        Categoria categoria = entityManager.find(Categoria.class,id);
        if(categoria!=null){
            return categoria;
        }
        return new Categoria("Producto con id:" + id + " no existe en sistema");
    }

    @Override
    public String deleteCategoriaById(int id){
        Categoria categoria = entityManager.find(Categoria.class,id);
        if (categoria!=null){
            entityManager.remove(categoria);
            return "Categoria: [" + categoria.getIdCategoria() + "] - " + categoria.getCategoria() + " Eliminado correctamente";
        }
        return "Categoria: ["+ id +"] no encontrado";
    }

    @Override
    public String registrarCategoria(Categoria categoria){
        try {
            entityManager.merge(categoria);
            return "Categoria: " + categoria.getCategoria() + " Creada Correctamente";
        }catch (Exception e){
            return "Categoria no Creada";
        }
    }

    @Override
    public String actualizarCategoria(int id, Categoria categoria){
        if (entityManager.find(Categoria.class,id)!=null){
            try {
                entityManager.merge(categoria);
                return "Categoria: " + id + " Actualizada Correctamente";
            }catch (Exception e){
                return "Categoria no Actualizada";
            }
        }
        return "Categoria inexistente, por tanto no se puede actualizar";
    }

}
