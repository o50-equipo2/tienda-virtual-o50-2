package com.tiendavirtual.TiendaVirtual.models;

import lombok.*;
import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "movimiento")
@ToString
@EqualsAndHashCode
public class Movimiento {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter @Setter @Column(name = "idmovimiento")
    private Integer idMovimiento;
    @Getter @Setter @Column(name = "idproducto")
    private Integer idProducto;
    @Getter @Setter @Column(name = "cantidad")
    private Integer cantidad;
    @Getter @Setter @Column(name = "valor")
    private Float valor;
    @Getter @Setter @Column(name = "fecha")
    private  Date fecha;
    @Getter @Setter @Column(name = "tipo")
    private String tipo;

    public Movimiento() {}

    public Movimiento(String tipo) {
        this.tipo = tipo;
    }

    public Movimiento(int idMovimiento, int idProducto, int cantidad, float valor, Date fecha, String tipo ) {
        this.idMovimiento = idMovimiento;
        this.idProducto = idProducto;
        this.cantidad = cantidad;
        this.valor = valor;
        this.fecha = fecha;
        this.tipo = tipo;
    }
}
