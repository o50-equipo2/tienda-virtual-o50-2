package com.tiendavirtual.TiendaVirtual.dao;

import com.tiendavirtual.TiendaVirtual.models.Producto;

import java.util.List;

public interface ProductoDao {

    List<Producto> getProductos();
    Producto getProductoById(int id);

    String deteleProductoById(int id);

    String registrarProducto(Producto producto);

    String actualizarProducto(int id, Producto producto);
}
