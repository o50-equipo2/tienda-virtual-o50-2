package com.tiendavirtual.TiendaVirtual.controllers;

import com.tiendavirtual.TiendaVirtual.dao.ProductoDao;
import com.tiendavirtual.TiendaVirtual.models.Producto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RestController
public class ProductoController {

    @Autowired
    private ProductoDao productoDao;

    @RequestMapping(value = "api/productos", method = RequestMethod.GET)
    public List<Producto> getProductos(){
        return productoDao.getProductos();
    }

    @RequestMapping(value="api/productos/{id}", method = RequestMethod.GET)
    public Producto getProductoById(@PathVariable int id){
        return productoDao.getProductoById(id);
    }


    @RequestMapping(value="api/productos/{id}", method = RequestMethod.DELETE)
    public String deleteProductoById(@PathVariable int id){
        return productoDao.deteleProductoById(id);
    }

    @RequestMapping(value = "api/productos", method = RequestMethod.POST)
    public String registrarProducto(@RequestBody Producto producto){
        return productoDao.registrarProducto(producto);
    }

    @RequestMapping(value = "api/productos/{id}", method = RequestMethod.PUT)
    public String actulizarProducto(@PathVariable int id, @RequestBody Producto producto){
        return productoDao.actualizarProducto(id, producto);
    }


}
