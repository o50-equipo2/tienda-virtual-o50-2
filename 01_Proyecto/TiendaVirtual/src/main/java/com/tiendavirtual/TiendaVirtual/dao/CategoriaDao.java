package com.tiendavirtual.TiendaVirtual.dao;

import com.tiendavirtual.TiendaVirtual.models.Categoria;
import java.util.List;

public interface CategoriaDao {

    List<Categoria> getCategorias();

    Categoria getCategoriaById(int id);

    String deleteCategoriaById(int id);

    String registrarCategoria(Categoria categoria);

    String actualizarCategoria(int id, Categoria categoria);

}
