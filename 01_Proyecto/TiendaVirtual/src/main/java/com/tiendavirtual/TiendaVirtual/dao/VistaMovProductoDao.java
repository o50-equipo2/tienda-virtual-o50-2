package com.tiendavirtual.TiendaVirtual.dao;


import com.tiendavirtual.TiendaVirtual.models.VistaMovProducto;

import java.util.List;

public interface VistaMovProductoDao {
    List<VistaMovProducto> getMovProductos();
    List<VistaMovProducto> getMovProductosCompras();
    List<VistaMovProducto> getMovProductosVentas();
    VistaMovProducto getMovimientoById(int id);
}
