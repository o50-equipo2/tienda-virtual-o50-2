package com.tiendavirtual.TiendaVirtual.dao;

import com.tiendavirtual.TiendaVirtual.models.Producto;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
@Transactional
public class ProductoDaoImp implements ProductoDao{

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Producto> getProductos() {
        String query = "FROM Producto";
        return entityManager.createQuery(query).getResultList();
    }

    @Override
    public Producto getProductoById(int id) {
        Producto producto = entityManager.find(Producto.class,id);
        if(producto!=null){
            return producto;

        }
        return new Producto("Producto con id:" + id + " no existe en sistema");
    }

    @Override
    public String deteleProductoById(int id) {
        Producto producto= entityManager.find(Producto.class,id);
        if(producto!=null){
            entityManager.remove(producto);
            return "Producto: [" + producto.getIdProducto() + "] - " + producto.getNombre() + " Eliminado correctamente";
        }
        return "Producto: ["+ id +"] no encontrado";
    }

    @Override
    public String registrarProducto(Producto producto) {
        try {
            entityManager.merge(producto);
            return "Producto: " + producto.getNombre() + " Creado Correctamente";
        }catch (Exception e){
            return "Producto no Creado";
        }
    }

    @Override
    public String actualizarProducto(int id, Producto producto) {
        if(entityManager.find(Producto.class,id)!=null){
            try {
                entityManager.merge(producto);
                return "Producto: " + id + " Actualizado Correctamente";
            }catch (Exception e){
                return "Producto no Actualizado";
            }
        }
        return "Producto inexistente, por tanto no se puede actualizar";
    }
}
