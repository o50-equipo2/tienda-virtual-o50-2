package com.tiendavirtual.TiendaVirtual.models;

import lombok.*;
import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "vistamovproducto") ///PVR en minuscula o interpreta Mayuscula como guion
@ToString
@EqualsAndHashCode
public class VistaMovProducto {
    @Id
    //@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter
    @Column(name = "idmovimiento")
    private Integer idMovimiento;
    @Getter @Column(name = "idproducto")
    private Integer idProducto;
    @Getter @Column(name = "cantidad")
    private Integer cantidad;
    @Getter @Column(name = "valor")
    private Float valor;
    @Getter @Column(name = "fecha")
    private Date fecha;
    @Getter  @Column(name = "tipo")
    private String tipo;
    @Getter @Column(name = "nombre")
    private String nombre;
    @Getter @Column(name = "idcategoria")
    private Integer idCategoria;
    @Getter @Column(name = "categoria")
    private String categoria;

    public VistaMovProducto() {}
    public VistaMovProducto(String tipo) {
        this.tipo = tipo;
    }

    public VistaMovProducto(int idMovimiento, int idProducto, int cantidad, float valor, Date fecha, String tipo, String nombre, int idCategoria, String categoria ) {
        this.idMovimiento = idMovimiento;
        this.idProducto = idProducto;
        this.cantidad = cantidad;
        this.valor = valor;
        this.fecha = fecha;
        this.tipo = tipo;
        this.nombre = nombre;
        this.idCategoria = idCategoria;
        this.categoria = categoria;
    }
}
