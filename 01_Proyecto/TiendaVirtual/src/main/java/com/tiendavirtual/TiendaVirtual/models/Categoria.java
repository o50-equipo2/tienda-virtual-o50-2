package com.tiendavirtual.TiendaVirtual.models;


import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;


@Entity
@Table(name = "categoria")
@ToString @EqualsAndHashCode
public class Categoria {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter @Setter @Column(name = "idcategoria")
    private int idCategoria;

    @Getter @Setter @Column(name = "categoria")
    private String categoria;

    public Categoria(){};

    public Categoria(String nombre){
        this.categoria = categoria;
    }

    public Categoria(int idCategoria, String categoria) {
        this.idCategoria = idCategoria;
        this.categoria = categoria;
    }
}
