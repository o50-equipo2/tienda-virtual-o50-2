package com.tiendavirtual.TiendaVirtual.dao;

import com.tiendavirtual.TiendaVirtual.models.Movimiento;

import java.util.List;

public interface MovimientoDaoVentas {
    List<Movimiento> getMovimientos();
    Movimiento getMovimientoById(int id);
    String borrarMovimientoById(int id);

    String registrarMovimiento(Movimiento movimiento);

    String actualizarMovimiento(int id, Movimiento movimiento);
}
