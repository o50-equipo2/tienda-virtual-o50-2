package com.tiendavirtual.TiendaVirtual.controllers;

import com.tiendavirtual.TiendaVirtual.dao.VistaMovProductoDao;
import com.tiendavirtual.TiendaVirtual.models.VistaMovProducto;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class VistaMovProductoController {
    @Autowired
    private VistaMovProductoDao movProductoDao;

    @RequestMapping(value = "api/movproducto", method = RequestMethod.GET)
    public List<VistaMovProducto> getMovProductos(){
        return movProductoDao.getMovProductos();
    }
    @RequestMapping(value="api/movproducto/{id}", method = RequestMethod.GET)
    public VistaMovProducto getMovimientoById(@PathVariable int id){
        return movProductoDao.getMovimientoById(id);
    }
    @RequestMapping(value = "api/movproductocompras", method = RequestMethod.GET)
    public List<VistaMovProducto> getMovProductosCompras(){
        return movProductoDao.getMovProductosCompras();
    }
    @RequestMapping(value = "api/movproductoventas", method = RequestMethod.GET)
    public List<VistaMovProducto> getMovProductosVentas(){
        return movProductoDao.getMovProductosVentas();
    }
}
