package com.tiendavirtual.TiendaVirtual.controllers;

import com.tiendavirtual.TiendaVirtual.dao.MovimientoDao;
import com.tiendavirtual.TiendaVirtual.models.Movimiento;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
public class MovimientoController {

    @Autowired
    private MovimientoDao movimientoDao;

    @RequestMapping(value = "api/movimientos", method = RequestMethod.GET)
    public List<Movimiento> getMovimientos(){
        return movimientoDao.getMovimientos();
    }

    @RequestMapping(value="api/movimientos/{id}", method = RequestMethod.GET)
    public Movimiento getMovimientoById(@PathVariable int id){
        return movimientoDao.getMovimientoById(id);
    }
    @RequestMapping(value="api/movimientos/{id}", method = RequestMethod.DELETE)
    public String borrarMovimientoById(@PathVariable int id){
        return movimientoDao.borrarMovimientoById(id);
    }

    @RequestMapping(value = "api/movimientos", method = RequestMethod.POST)
    public String registrarMovimiento(@RequestBody Movimiento movimiento){
        return movimientoDao.registrarMovimiento(movimiento);
    }

    @RequestMapping(value = "api/movimientos/{id}", method = RequestMethod.PUT)
    public String actualizarMovimiento(@PathVariable int id, @RequestBody Movimiento movimiento){
        return movimientoDao.actualizarMovimiento(id, movimiento);
    }
}
