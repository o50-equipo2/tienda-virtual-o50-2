package com.tiendavirtual.TiendaVirtual.dao;

import com.tiendavirtual.TiendaVirtual.models.Movimiento;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
@Transactional
public class MovimientoDaoComprasImp implements MovimientoDaoCompras {
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Movimiento> getMovimientos() {
        String query = "FROM Movimiento WHERE tipo = 'COMPRA'";
        return entityManager.createQuery(query).getResultList();
    }
    @Override
    public Movimiento getMovimientoById(int id) {
        Movimiento movimiento = entityManager.find(Movimiento.class,id);
        if(movimiento!=null) {
            return movimiento;
        }
        return new Movimiento("NO HAY"); // PVR equivalente a Null
    }

    @Override
    public String borrarMovimientoById(int id) {
        Movimiento movimiento= entityManager.find(Movimiento.class,id);
        if(movimiento!=null){
            entityManager.remove(movimiento);
            return "Movimiento: [" + movimiento.getIdMovimiento() + "] Eliminado correctamente";
        }
        return "Movimiento con id:" + id + " no existe en sistema";
    }

    @Override
    public String registrarMovimiento(Movimiento movimiento) {
        try {
            movimiento = entityManager.merge(movimiento);
            return "Movimiento Compras: " + movimiento.getIdMovimiento() + " Creado Correctamente";
        }catch (Exception e){
            return "Movimiento no Creado";
        }
    }

    @Override
    public String actualizarMovimiento(int id, Movimiento movimiento) {
        if(entityManager.find(Movimiento.class,id)!=null){
            try {
                entityManager.merge(movimiento);
                return "Movimiento Compras: " + id + " Actualizado Correctamente";
            }catch (Exception e){
                return "Movimiento no Actualizado";
            }
        }
        return "Movimiento inexistente, por tanto no se puede actualizar";
    }

}
