package com.tiendavirtual.TiendaVirtual.models;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "producto")
@ToString @EqualsAndHashCode
public class Producto {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter @Setter @Column(name = "idproducto")
    private Integer idProducto;

    @Getter @Setter @Column(name = "idcategoria")
    private Integer idCategoria;

    @Getter @Setter @Column(name = "codigo")
    private String codigo;

    @Getter @Setter @Column(name = "cantidad")
    private Integer cantidad;

    @Getter @Setter @Column(name = "nombre")
    private String nombre;

    @Getter @Setter @Column(name = "fechavencimiento")
    private Date fechaVencimiento;

    @Getter @Setter @Column(name = "valorcosto")
    private Float valorCosto;

    @Getter @Setter @Column(name = "valorventa")
    private Float valorVenta;

    public Producto(){};

    public Producto(String nombre) {
        this.nombre = nombre;
    }



    public Producto(int idProducto, int idCategoria, String codigo, int cantidad, String nombre, Date fechaVencimiento, float valorCosto, float valorVenta) {
        this.idProducto = idProducto;
        this.idCategoria = idCategoria;
        this.codigo = codigo;
        this.cantidad = cantidad;
        this.nombre = nombre;
        this.fechaVencimiento = fechaVencimiento;
        this.valorCosto = valorCosto;
        this.valorVenta = valorVenta;
    }
}
