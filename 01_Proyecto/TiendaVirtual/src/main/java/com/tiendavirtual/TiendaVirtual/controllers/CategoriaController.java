package com.tiendavirtual.TiendaVirtual.controllers;

import com.tiendavirtual.TiendaVirtual.dao.CategoriaDao;
import com.tiendavirtual.TiendaVirtual.models.Categoria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class CategoriaController {

    @Autowired
    private CategoriaDao categoriaDao;

    @GetMapping(value = "api/categorias")
    public List<Categoria> getCategorias(){
        return categoriaDao.getCategorias();
    }

    @GetMapping("api/categorias/{id}")
    public Categoria getCategoriaById(@PathVariable int id){
        return categoriaDao.getCategoriaById(id);
    }

    @DeleteMapping("api/categorias/{id}")
    public String deleteProductoById(@PathVariable int id){
        return categoriaDao.deleteCategoriaById(id);
    }

    @PostMapping("api/categorias")
    public String registrarCategoria(@RequestBody Categoria categoria){
        return categoriaDao.registrarCategoria(categoria);
    }

    @PutMapping("api/categorias/{id}")
    public String actualizarCategoria(@PathVariable int id, @RequestBody Categoria categoria){
        return categoriaDao.actualizarCategoria(id, categoria);
    }
}
