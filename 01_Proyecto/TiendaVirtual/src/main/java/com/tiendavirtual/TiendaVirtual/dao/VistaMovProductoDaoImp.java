package com.tiendavirtual.TiendaVirtual.dao;

import com.tiendavirtual.TiendaVirtual.models.VistaMovProducto;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public class VistaMovProductoDaoImp implements VistaMovProductoDao {
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<VistaMovProducto> getMovProductos() {
        String query = "FROM VistaMovProducto"; //PVR exacto con en la Base de Datos
        return entityManager.createQuery(query).getResultList();
    }

    @Override
    public List<VistaMovProducto> getMovProductosCompras() {
        String query = "FROM VistaMovProducto WHERE tipo ='COMPRA'";
        return entityManager.createQuery(query).getResultList();
    }

    @Override
    public List<VistaMovProducto> getMovProductosVentas() {
        String query = "FROM VistaMovProducto WHERE tipo ='VENTA'";
        return entityManager.createQuery(query).getResultList();
    }

    @Override
    public VistaMovProducto getMovimientoById(int id) {
        VistaMovProducto movimiento = entityManager.find(VistaMovProducto.class,id);
        if(movimiento!=null) {
            return movimiento;
        }
        return new VistaMovProducto("NO HAY"); // PVR equivalente a Null
    }
}
